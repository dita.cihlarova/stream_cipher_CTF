#!/usr/bin/python3

TEXT = './in/bis.txt'
GIF = './in/hint.gif'
PY_CIPHER = './in/super_cipher.py'

SECRET = GIF

SUB = [0, 1, 1, 0, 1, 0, 1, 0]
N_B = 32
N = 8 * N_B


# PRNG function - get seed (integer) and return different integer
def step(x):
    x = (x & 1) << N + 1 | x << 1 | x >> N - 1
    result = 0
    for i in range(N):
        result |= SUB[(x >> i) & 7] << i
    return result


# check whether two leftmost bits of proposed solution are equal to two rightmost bits
def check_solution(intermediate_x):
    lower = intermediate_x & (0b00000011)
    higher = (intermediate_x & (0b00000011 << N)) >> N
    return lower == higher


# second part of reversed step function
def solve_with_option(returned_y, opt):
    result = opt
    for i in range(N-2, -1, -1):  # iterates from N-2 to 0 with step 1
        if returned_y & (2 ** i) == 0:  # if current bit is 0
            if result & 3 == 0: # if two rightmost bits of result are 00
                result <<= 1    # three rightmost bits: 000
            else:
                result <<= 1
                result |= 1     # three rightmost bits: xx1
        else:  # if current bit is 1
            if result & 3 == 0: # if two rightmost bits of result are 00
                result <<= 1
                result |= 1     # three rightmost bits: xx1
            else:
                result <<= 1    # three rightmost bits: 000
    return result


# reversed function step
def step_back(returned_y):
    if returned_y & (2 ** (N - 1)) == 0:    # if current bit is 0
        for i in [0, 3, 5, 7]:  # try bits 000, 011, 101, 111
            intermediate_x = solve_with_option(returned_y, i)
            if check_solution(intermediate_x):
                break
    else:    # if current bit is 1
        for i in [1, 2, 4, 6]:  # try bits 001, 010, 100, 110
            intermediate_x = solve_with_option(returned_y, i)
            if check_solution(intermediate_x):
                break

    x = (intermediate_x >> 1) & ((1 << N) - 1)
    return x


# take two arrays of bytes, do bitwise XOR and return one array of bytes
def xor(a, b):
    result = []
    for i, j in zip(a, b):
        result.append(i ^ j)
    return result


# open files with plaintext, corresponding ciphertext and the encrypted python file
with open(TEXT, "rb") as f:
    plaintext = f.read()
with open(TEXT+'.enc', "rb") as f:
    ciphertext = f.read()
with open(SECRET + '.enc', 'rb') as f:
    secret_enc = f.read()

# xor plaintext with ciphertext to get keystream
basic_keystream = xor(plaintext, ciphertext)[:N_B]
# convert keystream from bytes to integer
keystream_int = int.from_bytes(basic_keystream, byteorder='little')

for i in range(N//2):	# run 128x
    keystream_int = step_back(keystream_int)

print(keystream_int.to_bytes(N_B, byteorder='little').decode('ascii'), end='')

####create a file for writing the result of decoded gif/python script
###with open(SECRET, 'wb') as secret_dec:
###
###    # decode 32B chunks of secret
###    for i in range(0, len(secret_enc), 32):
###        #xor keystream with equally large part of secret
###        part = xor(keystream_int.to_bytes(64, byteorder='little'), secret_enc[i:i + 32])
###        #write result to file
###        secret_dec.write(bytearray(part))
###        #get the next keystrem
###        keystream_int = step(keystream_int)
###
###    secret_dec.close()

