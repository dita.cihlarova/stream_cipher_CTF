#### Dita Cihlářová (xcihla02)
# KRY, projekt 1, dokumentace

Úkolem projektu bylo analyzovat danou proudovou šifru a získat tajemství. K dispozici byly čtyři soubory:

-   bis.txt – plaintext soubor o velikosti 512 B
    
-   bis.txt.enc – korespondující ciphertext
    
-   super-cipher.py.enc – zašifrovaný soubor o velikosti 714 B
    
-   hint.gif.enc – zašifrovaný soubor o velikosti 20,4 kB
    

## 1. Získání keystreamu
    

Keystream je posloupnost bytů vygenerovaná pseuonáhodným generátorem z původního klíče (seedu). Při použití synchronní proudové šifry získáme ciphertext tak, že provedeme operaci XOR s plaintextem a keystreamem. V tomto případě jsme měli k dispozici plaintext a ciphertext. Vzhledem k tomu, že XOR je reverzibilní funkce, je možné z této dvojice získat korespondující keystream.

## 2. Dekódování super_cipher.py
    

Získaný keystream lze využít pro operaci XOR s obsahem souboru super_cipher.py.enc. Výsledkem operace byla již čitelná část skriptu v jazyce Python 3. Tento postup fungoval, protože při šifrování souborů bis.txt i super_cipher.py byl použit stejný klíč, ze kterého vznikl stejný keystream, alespoň pro prvních 512 B. Získali jsme tedy prvních 512 znaků skriptu použitého k šifrování. Tato část obsahovala především funkci step, použitou pro generování keystreamu.

## 3. Dekódování hint.gif
    

Abychom mohli rozšifrovat hint.gif (a zbylých 202 B super_cipher.py), bylo potřeba vygenerovat delší keystream. To je možné s pomocí funkce step, opakované tak dlouho, dokud nezískáme dostatečný počet bytů, abychom s tímto prodlouženým keystreamem mohli provést operaci XOR s obsahem souboru hint.gif. Po dekódování se z obrázku zjistilo, že hledané tajemství je klíč použitý pro zašifrování původních souborů. Podařilo se tímto způsobem také rozšifrovat celý obsah souboru super_cipher.py, nicméně zbylý obsah nebyl k dalšímu postupu příliš přínosný.

## 4. Reverzace PRNG funkce
    

Jelikož potřebujeme získat klíč a známe funkci použitou k převedení klíče na keystream i korespondující keystream, můžeme se pokusit funkci reverzovat, použít na keystream a získat původní klíč. Funkce step má dvě části:

1.  Upravení vstupu x: Pokud bity vstupu x měly hodnotu a{b}c, po úpravě budou mít hodnotu ca{b}ca. Platí, že první a poslední dvojice bitů je stejná. Musíme také počítat s tím, že se x o dva bity prodlouží.
    
2.  Vytvoření nového čísla: Sérií kroků se s využitím x vytvoří výstupní hodnota y (pro detailní postup viz kód). Číslo y má vždy N bitů. Číslo x se v tomto kroku nijak nemění.
    

### Reverzace druhé části funkce

Reverzaci začínáme s hodnotou y, která má N bitů. Postupujeme od nejlevějšího bitu a pro každý bit hledáme jeho předobraz - část hodnoty x. Hodnota nula má čtyři možné předobrazy, hodnota jedna také. Nicméně stačí náhodně zvolit předobraz nejlevějšího bitu a poté už díky posuvům x máme vždy první dva bity předobrazu k dispozici, takže je stačí doplnit danou hodnotou; již nehádáme. Pro každé y tak provedeme nejvýše 4 pokusy k nalezení odpovídajícího x. Správnou hodnotu poznáme podle toho, že první a poslední dvojice bitů je stejná.

### Reverzace první části funkce

Nyní máme hodnotu, u které stačí odstranit první a poslední bit, posunout o jeden bit doprava a dostaneme původní x.

Protože při vytváření keystreamu se funkce step provedla N/2 krát, stejný počet opakování použijeme i pro reverzní funkci. Po posledním opakování máme výsledek - klíč. Stačí ho poté převést na byty a ascii text a získáme tajemství.

## 5. Závěr

Dva zašifrované soubory byly rozluštěny. Byla reverzována funkce pro generování keystreamu a odhalen klíč. Všechen zdrojový kód je v souboru solution.py.

Slabinou této šifry bylo především opakované použití klíče. Kdyby byl klíč unikátní pro každý soubor, nebylo by možné pomocí jednoho získaného keystreamu rozšifrovat nic dalšího.